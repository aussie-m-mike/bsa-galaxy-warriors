<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Command\ApplyReactorCommand;
use BinaryStudioAcademy\Game\Command\AttackCommand;
use BinaryStudioAcademy\Game\Command\BuyCommand;
use BinaryStudioAcademy\Game\Command\CommandRegistry;
use BinaryStudioAcademy\Game\Command\ExitCommand;
use BinaryStudioAcademy\Game\Command\GrabCommand;
use BinaryStudioAcademy\Game\Command\HelpCommand;
use BinaryStudioAcademy\Game\Command\RestartCommand;
use BinaryStudioAcademy\Game\Command\SetGalaxyCommand;
use BinaryStudioAcademy\Game\Command\StatsCommand;
use BinaryStudioAcademy\Game\Command\WhereAmICommand;
use BinaryStudioAcademy\Game\Contracts\Galaxy;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Entity\Galaxy\BattleGalaxy;
use BinaryStudioAcademy\Game\Entity\Galaxy\ExecutorGalaxy;
use BinaryStudioAcademy\Game\Entity\Galaxy\GalaxyRegistry;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Entity\Galaxy\PatrolGalaxy;
use BinaryStudioAcademy\Game\Entity\Spaceship\PlayerSpaceship;
use BinaryStudioAcademy\Game\Entity\Spaceship\Spaceship;

class Universe
{
    public $random;
    public $spaceship;

    private $isRunning;
    private $command;
    private $galaxies;

    protected $galaxy;

    public function __construct($random)
    {
        $this->isRunning = true;
        $this->random = $random;
        $this->command = new CommandRegistry();
        $this->galaxies = new GalaxyRegistry();
        $this->initGalaxies()->initUniverse()->initCommands();
    }

    public function executeCommand(Reader $reader, Writer $writer)
    {
        $input = trim($reader->read());

        $commandName = $this->parseCommand($input);
        $param = $this->parseParam($input);

        $command = $this->command->get($commandName);

        $writer->writeln($command->execute($param));
    }

    public function isRunning(): bool
    {
        return $this->isRunning;
    }

    public function getSpaceShip()
    {
        return $this->spaceship;
    }

    public function setSpaceShip(Spaceship $spaceship)
    {
        $this->spaceship = $spaceship;
    }

    public function setGalaxy(Galaxy $galaxy)
    {
        $this->galaxy = $galaxy;
    }

    public function getGalaxy()
    {
        return $this->galaxy;
    }

    public function getGalaxies()
    {
        return $this->galaxies;
    }

    public function exit()
    {
        $this->isRunning = false;
    }

    public function restart()
    {
        $this->initUniverse();
    }

    private function initUniverse()
    {
        $this->galaxy = $this->galaxies->get('home');
        $this->spaceship = new PlayerSpaceship(5, 5, 5, 100, []);

        return $this;
    }

    private function initGalaxies()
    {
        $this->galaxies->add(new HomeGalaxy(), 'home');

        $this->galaxies->add(new PatrolGalaxy('Andromeda', $this->random), 'andromeda');
        $this->galaxies->add(new PatrolGalaxy('Pegasus', $this->random), 'pegasus');
        $this->galaxies->add(new PatrolGalaxy('Spiral', $this->random), 'spiral');

        $this->galaxies->add(new BattleGalaxy('Shiar', $this->random), 'shiar');
        $this->galaxies->add(new BattleGalaxy('Xeno', $this->random), 'xeno');

        $this->galaxies->add(new ExecutorGalaxy('Isop'), 'isop');

        return $this;
    }

    private function initCommands()
    {
        $this->command->add(new HelpCommand(), 'help');
        $this->command->add(new StatsCommand($this), 'stats');
        $this->command->add(new SetGalaxyCommand($this), 'set-galaxy');
        $this->command->add(new AttackCommand($this), 'attack');
        $this->command->add(new BuyCommand($this), 'buy');
        $this->command->add(new ApplyReactorCommand($this), 'apply-reactor');
        $this->command->add(new WhereAmICommand($this), 'whereami');
        $this->command->add(new GrabCommand($this), 'grab');
        $this->command->add(new RestartCommand($this), 'restart');
        $this->command->add(new ExitCommand($this), 'exit');

        return $this;
    }

    private function parseCommand($input)
    {
        $args = explode(' ', $input);
        return $args[0];
    }

    private function parseParam($input)
    {
        $args = explode(' ', $input);
        return $args[1];
    }
}