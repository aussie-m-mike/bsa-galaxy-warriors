<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;

class Galaxy extends AbstractGalaxy
{
    public function getName()
    {
        return $this->name;
    }

    public function getSpaceship()
    {
        return $this->spaceship;
    }

}