<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;

use BinaryStudioAcademy\Game\Entity\Spaceship\PlayerSpaceship;

class HomeGalaxy extends Galaxy
{
    public function __construct()
    {
        $this->name = 'Home Galaxy';
        $this->spaceship = new PlayerSpaceship(5, 5, 5, 100, []);
    }
}