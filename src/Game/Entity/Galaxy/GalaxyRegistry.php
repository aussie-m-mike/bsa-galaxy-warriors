<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;

use BinaryStudioAcademy\Game\Contracts\Galaxy;

class GalaxyRegistry
{
    private $registry = [];

    public function add(Galaxy $galaxy, $name)
    {
        $this->registry[$name] = $galaxy;
    }

    public function get($name)
    {
        if (!isset($this->registry[$name])) {
            return 'Nah. No specified galaxy found.';
        }

        return $this->registry[$name];
    }
}