<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;


use BinaryStudioAcademy\Game\Entity\Spaceship\ExecutorSpaceship;
use BinaryStudioAcademy\Game\Entity\Spaceship\Spaceship;

class ExecutorGalaxy extends Galaxy
{
    public function __construct(String $name)
    {
        $this->name = $name;
        $this->createSpaceship();
    }

    public function createSpaceship()
    {
        $this->spaceship = new ExecutorSpaceship(10, 10, 10, 100,
            [Spaceship::ARTIFACT_REACTOR, Spaceship::ARTIFACT_CRYSTAL, Spaceship::ARTIFACT_CRYSTAL]);
    }
}