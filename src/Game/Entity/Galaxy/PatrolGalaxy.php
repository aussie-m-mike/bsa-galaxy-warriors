<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;

use BinaryStudioAcademy\Game\Entity\Spaceship\PatrolSpaceship;
use BinaryStudioAcademy\Game\Entity\Spaceship\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class PatrolGalaxy extends Galaxy
{
    public function __construct(String $name, Random $random)
    {
        $this->name = $name;
        $this->createSpaceship($random);
    }

    public function createSpaceship($random)
    {
        $this->generateStrength($random);
        $this->spaceship = new PatrolSpaceship(
            $this->generateStrength($random),
            $this->generateArmor($random),
            $this->generateLuck($random),
            100,
            [Spaceship::ARTIFACT_REACTOR]
        );
    }

    private function generateStrength($random)
    {
        $values = [3, 4];
        return $this->getRand($values, $random);
    }

    private function generateArmor($random)
    {
        $values = [2, 4];
        return $this->getRand($values, $random);
    }

    private function generateLuck($random)
    {
        $values = [1, 2];
        return $this->getRand($values, $random);
    }

    private function getRand($values, $random)
    {
        $rand = $random->get();
        if ($rand == 1) {
            return $values[$rand * count($values) - $rand];
        } else {
            return $values[$rand * count($values)];
        }
    }
}