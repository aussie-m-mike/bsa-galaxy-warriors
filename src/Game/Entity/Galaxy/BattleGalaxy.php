<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;


use BinaryStudioAcademy\Game\Entity\Spaceship\BattleSpaceship;
use BinaryStudioAcademy\Game\Entity\Spaceship\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class BattleGalaxy extends Galaxy
{
    public function __construct(String $name, Random $random)
    {
        $this->name = $name;
        $this->createSpaceship($random);
    }

    public function createSpaceship($random)
    {
        $this->spaceship = new BattleSpaceship(
            $this->generateStrength($random),
            $this->generateArmor($random),
            $this->generateLuck($random),
            100,
            [Spaceship::ARTIFACT_REACTOR, Spaceship::ARTIFACT_CRYSTAL]
        );
    }

    private function generateStrength($random)
    {
        $values = [5, 6, 7, 8];
        return $this->getRand($values, $random);
    }

    private function generateArmor($random)
    {
        $values = [6, 7];
        return $this->getRand($values, $random);
    }

    private function generateLuck($random)
    {
        $values = [3, 4, 5, 6];
        return $this->getRand($values, $random);
    }

    private function getRand($values, $random)
    {
        $rand = $random->get();
        if ($rand == 1) {
            return $values[$rand * count($values) - $rand];
        } else {
            return $values[$rand * count($values)];
        }
    }
}