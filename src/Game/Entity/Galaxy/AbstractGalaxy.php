<?php

namespace BinaryStudioAcademy\Game\Entity\Galaxy;

use BinaryStudioAcademy\Game\Contracts\Galaxy;

abstract class AbstractGalaxy implements Galaxy
{
    protected $name;
    protected $spaceship;

    abstract public function getName();
}