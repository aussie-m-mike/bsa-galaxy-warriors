<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;

use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

abstract class AbstractSpaceship implements Spaceship
{
    protected $name;
    protected $strength;
    protected $armor;
    protected $luck;
    protected $health;
    public $hold;

    abstract public function getName();

    abstract public function getStrength();

    abstract public function getArmor();

    abstract public function getLuck();

    abstract public function getHealth();

    abstract public function getHold();
}