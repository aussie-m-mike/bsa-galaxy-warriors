<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;

use BinaryStudioAcademy\Game\Contracts\Spaceship\ApplyReactor;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Buy;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Grab;
use BinaryStudioAcademy\Game\Helpers\Stats;

class PlayerSpaceship extends Spaceship implements ApplyReactor, Buy, Grab
{
    protected $name = 'Player spaceship';

    public function __construct(
        int $strength,
        int $armor,
        int $luck,
        int $health,
        array $hold
    ) {
        $this->strength = $strength;
        $this->armor = $armor;
        $this->luck = $luck;
        $this->health = $health;
        $this->hold = $hold;
    }

    public function applyReactor()
    {
        if ($this->hasReactor()) {
            if ($this->health < Stats::MAX_HEALTH - 20) {
                $this->health += 20;
            } else {
                $this->health = Stats::MAX_HEALTH;
            }

            $this->removeReactor();
        }
    }

    public function buy($param)
    {
        if ($this->hasCrystal()) {
            if ($param == 'reactor') {
                if ($this->hasSpace()) {
                    $this->addReactor();
                    $this->removeCrystal();
                    return "You've bought a magnet reactor. You have {$this->countStuff(Spaceship::ARTIFACT_REACTOR)} reactor(s) now." . PHP_EOL;
                } else {
                    return "You don't have enough space on the bay!";
                }
            } elseif ($param == 'armor' || $param == 'strength') {
                $this->removeCrystal();
                $this->upgradeSkill($param);
                return "You've got upgraded skill: {$param}. The level is {$this->$param} now." . PHP_EOL;
            } else {
                return "Invalid unit: {$param} provided";
            }
        } else {
            return "You don't have any " . Spaceship::ARTIFACT_CRYSTAL . "!" . PHP_EOL;
        }
    }

    public function grab(AbstractSpaceship $enemy)
    {
        if (!$enemy->isAlive() && count($enemy->hold)) {
            if (count($enemy->getHold())) {
                foreach ($enemy->hold as $artifact) {
                    if ($this->hasSpace()) {
                        $this->hold[] = $artifact;
                    } else {
                        return false;
                    }
                }

                $enemy->hold = [];
                return true;
            }
        }
    }

    private function hasReactor()
    {
        return in_array(Spaceship::ARTIFACT_REACTOR, $this->hold);
    }

    private function countStuff($type)
    {
        return array_count_values($this->hold)[$type];
    }

    private function removeReactor()
    {
        $key = array_search(Spaceship::ARTIFACT_REACTOR, $this->hold);
        if ($key !== false) {
            unset($this->hold[$key]);
        }
    }

    private function addReactor()
    {
        $this->hold[] = Spaceship::ARTIFACT_REACTOR;
    }

    private function hasCrystal()
    {
        return in_array(Spaceship::ARTIFACT_CRYSTAL, $this->hold);
    }

    private function removeCrystal()
    {
        $key = array_search(Spaceship::ARTIFACT_CRYSTAL, $this->hold);
        if ($key !== false) {
            unset($this->hold[$key]);
        }
    }

    private function upgradeSkill($skill)
    {
        if ($this->$skill < Stats::MAX_ARMOUR) {
            $this->$skill++;
        }
    }

    private function hasSpace()
    {
        return count($this->hold) < 3;
    }
}