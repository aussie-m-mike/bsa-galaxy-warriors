<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;

use BinaryStudioAcademy\Game\Contracts\Spaceship\ApplyReactor;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Buy;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Grab;

class ExecutorSpaceship extends Spaceship
{
    protected $name = 'Executor';

    public function __construct(
        int $strength,
        int $armor,
        int $luck,
        int $health,
        array $hold
    ) {
        $this->strength = $strength;
        $this->armor = $armor;
        $this->luck = $luck;
        $this->health = $health;
        $this->hold = $hold;
    }
}