<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;

use BinaryStudioAcademy\Game\Contracts\Spaceship\ApplyReactor;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Buy;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Grab;

class BattleSpaceship extends Spaceship
{
    protected $name = 'Battle AbstractSpaceship';

    public function __construct(
        int $strength,
        int $armor,
        int $luck,
        int $health,
        array $hold
    ) {
        $this->strength = $strength;
        $this->armor = $armor;
        $this->luck = $luck;
        $this->health = $health;
        $this->hold = $hold;
    }

    public function getArtifacts()
    {
        return Spaceship::ARTIFACT_REACTOR . ' ' . Spaceship::ARTIFACT_CRYSTAL;
    }
}