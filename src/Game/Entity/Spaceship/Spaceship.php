<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;


use BinaryStudioAcademy\Game\Helpers\Math;

class Spaceship extends AbstractSpaceship
{
    const ARTIFACT_CRYSTAL = '🔮';
    const ARTIFACT_REACTOR = '🔋';

    /**
     * @return int
     */
    public function getArmor(): int
    {
        return $this->armor;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    /**
     * @return string
     */
    public function getHoldText(): string
    {
        if (count($this->hold)) {
            return '[' . join(' ', $this->hold) . ']';
        } else {
            return '[ _ _ _ ]';
        }
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isAlive()
    {
        return $this->health > 0;
    }

    public function setDamage(int $points)
    {
        $this->health -= $points;
    }

    public function repair()
    {
        $this->health = 100;
    }

    public function attack(AbstractSpaceship $enemy, $random)
    {
        $damagePoints = Math::damage($this->strength, $enemy->armor);

        if (Math::luck($random, $this->luck)) {
            $enemy->setDamage($damagePoints);
            return $damagePoints;
        } else {
            return 0;
        }
    }
}