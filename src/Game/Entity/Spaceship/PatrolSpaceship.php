<?php

namespace BinaryStudioAcademy\Game\Entity\Spaceship;


class PatrolSpaceship extends Spaceship
{
    protected $name = 'Patrol AbstractSpaceship';

    public function __construct(
        int $strength,
        int $armor,
        int $luck,
        int $health,
        array $hold
    ) {
        $this->strength = $strength;
        $this->armor = $armor;
        $this->luck = $luck;
        $this->health = $health;
        $this->hold = $hold;
    }

    public function getArtifacts()
    {
        return Spaceship::ARTIFACT_REACTOR;
    }
}