<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    private $universe;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->universe = new Universe($random);
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Welcome on board space ranger!');
        $writer->writeln('Press enter to start... ');
        $reader->read();
        while ($this->universe->isRunning()) {
            $writer->writeln('Enter your command:');
            $this->universe->executeCommand($reader, $writer);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $this->universe->executeCommand($reader, $writer);
    }

}
