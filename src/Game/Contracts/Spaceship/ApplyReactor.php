<?php

namespace BinaryStudioAcademy\Game\Contracts\Spaceship;

interface ApplyReactor
{
    public function applyReactor();
}