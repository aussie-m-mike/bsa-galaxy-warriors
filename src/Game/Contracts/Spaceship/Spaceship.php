<?php

namespace BinaryStudioAcademy\Game\Contracts\Spaceship;

use BinaryStudioAcademy\Game\Entity\Spaceship\AbstractSpaceship;

interface Spaceship
{
    public function attack(AbstractSpaceship $enemy, $random);
}