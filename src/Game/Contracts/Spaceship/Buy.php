<?php

namespace BinaryStudioAcademy\Game\Contracts\Spaceship;

interface Buy
{
    function buy($param);
}