<?php

namespace BinaryStudioAcademy\Game\Contracts\Spaceship;

use BinaryStudioAcademy\Game\Entity\Spaceship\AbstractSpaceship;

interface Grab
{
    public function grab(AbstractSpaceship $enemy);
}