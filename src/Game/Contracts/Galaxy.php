<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Galaxy
{
    public function getSpaceship();

}