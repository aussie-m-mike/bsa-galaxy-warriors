<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Universe;

class ExitCommand implements Command
{
    protected $universe;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $this->universe->exit();
        return 'Bye bye!' . PHP_EOL;;
    }
}