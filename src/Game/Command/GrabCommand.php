<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Universe;

class GrabCommand implements Command
{
    protected $universe;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $galaxy = $this->universe->getGalaxy();

        if ($galaxy instanceof HomeGalaxy) {
            return 'Hah? You don\'t want to grab any staff at Home Galaxy. Believe me.' . PHP_EOL;
        } else {
            $enemySpaceship = $galaxy->getSpaceship();
            if ($enemySpaceship->isAlive()) {
                return 'LoL. Unable to grab goods. Try to destroy enemy spaceship first.' . PHP_EOL;
            } else {
                $spaceship = $this->universe->getSpaceShip();
                if ($spaceship->grab($enemySpaceship)) {
                    return 'You got ' . $enemySpaceship->getArtifacts() . '.';
                } else {
                    return 'You already collected artifacts or you don\'t have free space!';
                }
            }
        }
    }

}