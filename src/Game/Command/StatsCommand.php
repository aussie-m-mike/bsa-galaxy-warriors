<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Universe;

class StatsCommand implements Command
{
    private $universe;
    private $spaceship;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $this->spaceship = $this->universe->getSpaceship();

        return 'AbstractSpaceship stats:' . PHP_EOL
            . 'strength: ' . $this->spaceship->getStrength() . PHP_EOL
            . 'armor: ' . $this->spaceship->getArmor() . PHP_EOL
            . 'luck: ' . $this->spaceship->getLuck() . PHP_EOL
            . 'health: ' . $this->spaceship->getHealth() . PHP_EOL
            . 'hold: ' . $this->spaceship->getHoldText() . PHP_EOL;
    }
}