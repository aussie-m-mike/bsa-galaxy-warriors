<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Universe;

class ApplyReactorCommand implements Command
{
    protected $universe;
    protected $playerSpaceship;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $galaxy = $this->universe->getGalaxy();

        if ($galaxy instanceof HomeGalaxy) {
            return "Nah, you don\'t need it there!" . PHP_EOL;
        } else {
            $this->playerSpaceship = $this->universe->getSpaceShip();
            $this->playerSpaceship->applyReactor();
            return "Magnet reactor have been applied. Current spaceship health level is {$this->playerSpaceship->getHealth()}" . PHP_EOL;
        }
    }
}