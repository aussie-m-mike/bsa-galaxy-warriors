<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Universe;

class WhereAmICommand implements Command
{
    protected $universe;
    protected $galaxy;
    protected $spaceship;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $this->galaxy = $this->universe->getGalaxy();
        $this->spaceship = $this->universe->getGalaxy()->getSpaceship();

        return "Galaxy: {$this->galaxy->getName()}" . PHP_EOL;
    }
}