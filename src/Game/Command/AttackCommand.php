<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Entity\Spaceship\ExecutorSpaceship;
use BinaryStudioAcademy\Game\Entity\Spaceship\PlayerSpaceship;
use BinaryStudioAcademy\Game\Universe;

class AttackCommand implements Command
{
    protected $universe;
    protected $playerSpaceship;
    protected $enemySpaceship;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $this->playerSpaceship = $this->universe->getSpaceShip();
        $this->enemySpaceship = $this->universe->getGalaxy()->getSpaceship();
        $galaxy = $this->universe->getGalaxy();

        if ($galaxy instanceof HomeGalaxy) {
            return 'Calm down! No enemy spaceships detected. No one to fight with.' . PHP_EOL;;
        } else {
            $result = '';

            if (!$this->playerSpaceship->isAlive()) {
                return "Your spaceship got significant damages and eventually got exploded." . PHP_EOL
                    . "You have to start from Home Galaxy." . PHP_EOL;
            }

            //Attack enemy spaceship
            $pointsMe = $this->playerSpaceship->attack($this->enemySpaceship, $this->universe->random);

            //Check it could attack back
            if ($this->enemySpaceship->isAlive()) {
                //Strike back
                $points = $this->enemySpaceship->attack($this->playerSpaceship, $this->universe->random);

                if (!$this->playerSpaceship->isAlive()) {
                    $result .= "Your spaceship got significant damages and eventually got exploded." . PHP_EOL
                        . "You have to start from Home Galaxy." . PHP_EOL;

                    $this->universe->setSpaceShip(new PlayerSpaceship(5, 5, 5, 100, []));
                    $command = new SetGalaxyCommand($this->universe);
                    $command->execute('home');
                    return $result;
                }
                $result .= "{$this->enemySpaceship->getName()} has damaged on: {$pointsMe} points." . PHP_EOL;
                $result .= "health: {$this->enemySpaceship->getHealth()}" . PHP_EOL;

                $result .= "{$this->enemySpaceship->getName()} damaged your spaceship on: {$points} points." . PHP_EOL;
                $result .= "health: {$this->playerSpaceship->getHealth()}" . PHP_EOL;
            } else {
                if ($this->enemySpaceship instanceof ExecutorSpaceship) {
                    $result .= '🎉🎉🎉 Congratulations 🎉🎉🎉' . PHP_EOL
                        . '🎉🎉🎉 You are winner! 🎉🎉🎉';
                } else {
                    $result .= "{$this->enemySpaceship->getName()} is totally destroyed. Hurry up! There is could be something useful to grab." . PHP_EOL;
                }
            }

            return $result;
        }
    }
}