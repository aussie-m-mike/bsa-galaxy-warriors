<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Galaxy;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Universe;

class SetGalaxyCommand implements Command
{
    private $galaxies;
    private $universe;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
        $this->galaxies = $this->universe->getGalaxies();
    }

    public function execute($param)
    {
        $galaxy = $this->galaxies->get($param);
        if ($galaxy instanceof Galaxy) {
            $this->universe->setGalaxy($galaxy);

            if ($this->universe->getGalaxy() instanceof HomeGalaxy) {

                $this->universe->spaceship->repair();

                return "Galaxy: {$galaxy->getName()}." . PHP_EOL;
            } else {

                $galaxy->createSpaceship($this->universe->random);

                return "Galaxy: {$galaxy->getName()}." . PHP_EOL
                    . "You see a {$galaxy->getSpaceShip()->getName()}: " . PHP_EOL
                    . "strength: {$galaxy->getSpaceShip()->getStrength()}" . PHP_EOL
                    . "armor: {$galaxy->getSpaceShip()->getArmor()}" . PHP_EOL
                    . "luck: {$galaxy->getSpaceShip()->getLuck()}" . PHP_EOL
                    . "health: {$galaxy->getSpaceShip()->getHealth()}" . PHP_EOL;
            }
        } else {
            return $galaxy;
        }
    }
}