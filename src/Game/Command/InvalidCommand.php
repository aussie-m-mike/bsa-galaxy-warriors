<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;

class InvalidCommand implements Command
{
    private $name;

    public function __construct(String $name)
    {
        $this->name = $name;
    }

    public function execute($param)
    {
        return "Command '{$this->name}' not found" . PHP_EOL;
    }
}