<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Entity\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Universe;

class BuyCommand implements Command
{
    protected $universe;
    protected $playerSpaceship;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $galaxy = $this->universe->getGalaxy();

        if ($galaxy instanceof HomeGalaxy) {
            if ($param) {
                $this->playerSpaceship = $this->universe->getSpaceShip();
                return $this->playerSpaceship->buy($param);
            } else {
                return 'Please specify what you want to buy!' . PHP_EOL;
            }
        } else {
            return "You can buy stuff only in Home Galaxy!" . PHP_EOL;
        }
    }
}