<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;

class CommandRegistry
{
    private $registry = [];

    public function add(Command $command, $name)
    {
        $this->registry[$name] = $command;
    }

    public function get($name)
    {
        if (!isset($this->registry[$name])) {
            return new InvalidCommand($name);
        }

        return $this->registry[$name];
    }
}