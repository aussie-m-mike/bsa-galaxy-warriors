<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Universe;

class RestartCommand implements Command
{
    protected $universe;

    public function __construct(Universe $universe)
    {
        $this->universe = $universe;
    }

    public function execute($param)
    {
        $this->universe->restart();
        return 'Game has been restarted!' . PHP_EOL;
    }
}